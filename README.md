# Free LLMs API

A free toolkit which utilize API provided by [Le Chat Mistral](https://chat.mistral.ai) &  [Coze](https://www.coze.com).

## Getting started

```
pip install -e .
```

## How to use
### Mistral Large
```python
from llm_api import mistral_api_response

# Call with default configuration - Not Recommend
mistral_api_response(text="Introduce your self")

# Call with user configuration
CONFIGURATION = {
    "CSRF_TOKEN" : '',
    "ORY_KRATOS" : '',
    "ORY_SESTION" : '',
    "SENTRY_TRACE" : '',
    "BAGGAGE": '',
}
mistral_api_response(text="Introduce your self", configuration=CONFIGURATION)
```
### Coze GPT4
 ```python
from llm_api import gpt4_api_response

# Call with default configuration - Not Recommend
gpt4_api_response(text="Introduce your self")

# Call with user configuration
CONFIGURATION = {
    "bot_id": '',
    "api_id": '',
    "api_hash": '',
    "phone": '',
    "database_encryption_key": '',
}
gpt4_api_response(text="Introduce your self", configuration=CONFIGURATION)
```
### Configuration documentation
Please follow [this documentation](https://creative-tuba-f34.notion.site/C-ch-l-y-configure-cho-t-ng-t-i-kho-n-4d8c855a6fb142209ac3e01b7e529b93?pvs=4) to your configuration. 