import threading
import time
from telegram.client import Telegram
from llm_api.coze.config import CONFIGURATION

def send_msg(tg, text, bot_id):
    # if this is the first run, library needs to preload all chats
    # otherwise the message will not be sent
    result = tg.get_chats()

    # `tdlib` is asynchronous, so `python-telegram` always returns you an `AsyncResult` object.
    # You can wait for a result with the blocking `wait` method.
    result.wait()

    # if result.error:
    #     print(f'get chats error: {result.error_info}')
    # else:
    #     print(f'chats: {result.update}')

    sent_message_result = tg.send_message(
        chat_id=bot_id,
        text=text,
    )
    sent_message_result.wait()

    if sent_message_result.error:
        print(f'Failed to send the message: {sent_message_result.error_info}')

    # When python-telegram sends a message to tdlib,
    # it does not send it immediately. When the message is sent, tdlib sends an updateMessageSendSucceeded event.

    message_has_been_sent = threading.Event()

    # The handler is called when the tdlib sends updateMessageSendSucceeded event
    def update_message_send_succeeded_handler(update):
        # print(f'Received updateMessageSendSucceeded: {update}')
        # When we sent the message, it got a temporary id.
        # In the event we can also find the new id of the message.
        #
        # Check that this event is for the message we sent.
        if update['old_message_id'] == sent_message_result.update['id']:
            message_id = update['message']['id']
            message_has_been_sent.set()

    # When the event is received, the handler is called.
    tg.add_update_handler('updateMessageSendSucceeded', update_message_send_succeeded_handler)
    # Wait for the message to be sent
    message_has_been_sent.wait(timeout=60)

    # if result.error:
    #     print(f'Send message error: {result.error_info}')
    # else:
    #     print(f'Message has been sent.')
    
    
def gpt4_api_response(text, configuration=CONFIGURATION):
    
    tg = Telegram(
        api_id=configuration["api_id"],
        api_hash=configuration["api_hash"],
        phone=configuration["phone"],
        database_encryption_key=configuration["database_encryption_key"],
    )
    tg.login()
    
    send_msg(tg, text, bot_id=configuration["bot_id"])
    
    i = 0
    latency = 10
    final_message = ''
    start = time.time()
    
    while True:
        if i == 0:
            time.sleep(15)
        elif i < 5:
            time.sleep(latency)
        else:
            tg.stop()
            return ''
        
        def get_chat():
            tg.get_chats()

            #get msg
            response = tg.get_chat_history(
                chat_id=configuration["bot_id"],
                limit=2,
                from_message_id=0
            )
            response.wait()

            recent_message = [message['content']['text']['text'] for message in response.update['messages']]
            return recent_message
        
        recent_message = get_chat()

        if recent_message[-1] == text:
            final_message = recent_message[0]

            while True:
                time.sleep(15)
                recent_message = get_chat()
                if final_message != recent_message[0]:
                    final_message = recent_message[0]
                else:
                    tg.stop()
                    return recent_message[0]

                if time.time() - start > 200:
                    tg.stop()
                    return ""
            # return recent_message[0]
        i+=1
