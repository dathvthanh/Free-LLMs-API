BASE_COOKIES = {
    'csrf_token_1d61ec8f0158ec4868343239ec73dbe1bfebad9908ad860e62f470c767573d0d': "",
    '_cfuvid': '9aGPbpB5U..SQCN6VTQKAg_tpVbMhiq82SJzR8hy8mw-1709540320153-0.0.1.1-604800000',
    'ory_kratos_continuity': "",
    'ory_session_coolcurranf83m3srkfl': "",
    'NEXT_LOCALE': 'en',
}

BASE_HEADERS = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:122.0) Gecko/20100101 Firefox/122.0',
    'Accept': '*/*',
    'Accept-Language': 'en-US,en;q=0.5',
    'Referer': 'https://chat.mistral.ai/chat/',
    'content-type': 'application/json',
    'trpc-batch-mode': 'stream',
    'sentry-trace': "",
    'baggage': "",
    'Connection': 'keep-alive',
    'Sec-Fetch-Dest': 'empty',
    'Sec-Fetch-Mode': 'cors',
    'Sec-Fetch-Site': 'same-origin',
}

API_INFORMATION = {
    "CSRF_TOKEN" : '0sB+OPpjg34HMDUu2cAfecJ1Su/95sviSFFygzF8RhI=',
    "ORY_KRATOS" : 'MTcwOTY5MzM4MnxEWDhFQVFMX2dBQUJFQUVRQUFBRV80QUFBQT09fBsl3unM4o5oo4VjaUYJoVCAVvuPughGbkz7-4TtvVsO',
    "ORY_SESTION" : 'MTcwOTY5MzM4NHx6T1FINk5HYzN3cWlENG9OT3lJV2I0R0ZtOE9fTzNjaUpfWThZdF9BazFRbnBuc0llaEd6RjktdE9iX1diR2RRcEVESUdkU0RhLW5NZjJXdDFjWTlaY20zN0ZrYVdzUmVvalgtMzRLSmdjVklGVWd4NVFLMFFkZHIzUXBTUXRCVHpQa3praUxuWHpVVVppTC05WjdTZC1oSWp2ODE1dVJOM25iQnNxT21Dbl9QWHRVRGVZQjRlU0l2Si0teVhxOEtwRzRkTzRnVEJoWGRuZkZ2cko1Tlk4b2MwRUx3dHIzeThpdmFmR3NxQnF6d0wxNmNTU1YxcllFaEZNbGlVdnVNaHBZdlB2LW5ycGFwVVlBUEZuSE18Qonric8ZdmR5VEFiDKviR46untOYvW8_S_gO98KsmYw=',
    "SENTRY_TRACE" : 'c63d3778c58b435a8dbaf8faa2b762e6-9f92f5397b6603e7',
    "BAGGAGE": 'sentry-environment=production,sentry-release=0bcd5685fa38e23cf0eff889ad3d5c6d9acf3f35,sentry-public_key=4393092f302ba98235708d010cab4517,sentry-trace_id=c63d3778c58b435a8dbaf8faa2b762e6',
}

def get_configuration(api_information=API_INFORMATION):
    BASE_COOKIES["csrf_token_1d61ec8f0158ec4868343239ec73dbe1bfebad9908ad860e62f470c767573d0d"] = api_information["CSRF_TOKEN"]
    BASE_COOKIES["ory_kratos_continuity"] = api_information["ORY_KRATOS"]
    BASE_COOKIES["ory_session_coolcurranf83m3srkfl"] = api_information["ORY_SESTION"]
    BASE_HEADERS["sentry-trace"] = api_information["SENTRY_TRACE"]
    BASE_HEADERS["baggage"] = api_information["BAGGAGE"]
    return BASE_COOKIES, BASE_HEADERS