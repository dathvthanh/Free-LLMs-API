import os
import json
import requests
import argparse
import time
from llm_api.mistral.config import get_configuration, API_INFORMATION

# Create new message request
def create_message_request(message, cookies, headers):
    params = {
        'batch': '1',
    }

    json_data = {
        '0': {
            'json': {
                'content': message,
                'rag': False,
            },
        },
    }

    response = requests.post(
        'https://chat.mistral.ai/api/trpc/message.newChat',
        params=params,
        cookies=cookies,
        headers=headers,
        json=json_data,
    )
    
    return json.loads(response.content)["0"]["result"]["data"]["json"]["messages"][0]["chatId"]

def send_message_request(message, chatid, cookies, headers):
    json_data = {
        'messages': [
            {
                'role': 'user',
                'content': message,
            },
        ],
        'chatId': chatid,
        'model': 'mistral-large',
        'mode': 'retry',
    }

    response = requests.post('https://chat.mistral.ai/api/chat', cookies=cookies, headers=headers, json=json_data)
    
    if response.status_code != 200:
        return "Error"
    return "Success"
   
def get_message_answer(chatid, cookies, headers):
    params = {
        'batch': '1',
        'input': '{"0":{"json":{"chatId":"' + str(chatid) + '","cursor":null},"meta":{"values":{"cursor":["undefined"]}}}}',
    }

    response = requests.get('https://chat.mistral.ai/api/trpc/message.all', params=params, cookies=cookies, headers=headers)
    
    data_reponse = json.loads(response.content)
    
    return data_reponse["0"]["result"]["data"]["json"]["items"][1]["content"]
    
def mistral_api_response(text, configures=None):
    
    COOKIES, HEADERS = get_configuration(configures) if configures else get_configuration()

    chatid = create_message_request(text, COOKIES, HEADERS)
    if send_message_request(text, chatid, COOKIES, HEADERS) == "Success":
        answer = get_message_answer(chatid, COOKIES, HEADERS)
        return(answer)
    else:
        return("")
