from setuptools import setup, find_packages

with open("requirements.txt") as requirement_file:
    requirements = requirement_file.read().split()

setup(
    name="FreeLLMAPI",
    description="A package for calling Mistral, GPT4.",
    version="1.0.0",
    author="BrwonyEyez",
    author_email="dat.hvthanh@gmail.com",
    install_requires=requirements,
    packages=find_packages(exclude=["llm_api"]), # package = any folder with an __init__.py file
)